WinUtils
=======================

This is all the utils for windows to help users to fight against viruses and unexpected actions, such as changing your homepage.
There will be more utils coming out, to help against viruses!

Current Version
====================
This could help user restore the homepage changed by viruses on IE browsers (Currently only on IE).

Note
====================
You can run the executables under Executables folder, extract it, and run restoreHompage executable under dist folder.
When you run executables, you don't need a python interpreter installed on your machine.

Required Python Version if run from Source
===========================================
Python 2.7+ and Python 3.0-

Last Updated: Jan 13, 2014 