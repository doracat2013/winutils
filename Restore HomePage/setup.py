#-------------------------------------------------------------------------------
# Name:        Executable Setup Module
# Purpose:     This scripts could convert my winutils (RestoreHomePage) into windows executables.
#
# Version:     1.3a
#
# Author:      S Xia
#
# Created:     01/13/2014
# Copyright:   (C) S Xia 2014
# py2exe Copyright: See License.txt for py2exe license
# Licence:     You are free to use this software, but please do not redistribute it.
#-------------------------------------------------------------------------------

## Import Statements
from distutils.core import setup
import py2exe
import sys
####################

### Set env command
sys.argv.append("py2exe")
###################

### Setup Options
setup(
    options = {'py2exe': {'bundle_files': 1, 'compressed': True}},
    windows = [{'script': "restoreHomepage.py"}],
    zipfile = None,
)
################

## Core Compile Module
def compileCore():
    setup(console=['restoreHomePage.py'])
    pass
######################

### Start main function
def main():
    compileCore()
    pass
#######################

### Universal Controller
if __name__ == '__main__':
    main()
########################