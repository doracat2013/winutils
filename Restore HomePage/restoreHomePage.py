#-------------------------------------------------------------------------------
# Name:        RestoreHomePage
# Purpose:     This util can help restore the homepage in both ways, if the browser has been changed
#              by maleware, or anti-virus. This util can also change back the homepage even if the computer has been
#              infected by viruses.
#              Currently only support IE 8.0+.
#              This could also unlock the registry if it is locked by virus. (Still Under Development)
#              Killing AV software is not supported in this version.
#
# Note:        Please manual close the anti-virus might prevent changing homepage first.
#              Just right click, and click exit.
#
# Version:     1.3b
# PyVersion:   2.7+ 3.0-
#
# Author:      S Xia
#
# Created:     01/12/2014
# Copyright:   (c) S Xia 2014
# Licence:     You are free to use this software, but please do not redistribute this software.
#-------------------------------------------------------------------------------

## Import Statements
import _winreg as regctrl
####################

## This will restore the normal block
def restoreReg1(desiredURL):
    mykey = regctrl.OpenKey(regctrl.HKEY_CURRENT_USER, "Software\\Microsoft\\Internet Explorer\\Main",0,regctrl.KEY_ALL_ACCESS)
    try:
        regctrl.SetValueEx(mykey, 'Start Page', 0, regctrl.REG_SZ, desiredURL)
    except WindowsError:
        print "Error: Your security software or some running applications (Probably Maleware or Antivirus) try to prevent the changing of homepage! Please Close it First! \n Failed! "
    mykey.Close()
    pass

## This could restore the special way of changing homepage
def restoreReg2():
    mykey = regctrl.OpenKey(regctrl.HKEY_CLASSES_ROOT, "CLSID\\{871C5380-42A0-1069-A2EA-08002B30309D}\\shell\\OpenHomePage\\Command",0,regctrl.KEY_ALL_ACCESS)
    try:
        regctrl.SetValueEx(mykey, '', 0, regctrl.REG_SZ, "\"C:\\Program Files\\Internet Explorer\\iexplore.exe\"")
    except WindowsError:
        print "Unable to purely restore homepage, is your antivirus prevent this action? please contact author for an advanced version! "
    mykey.Close()
    pass

## Unlock the registry if it has been locked (Haven't been fully tested - Still Under Development)
def unlockReg():
    lockFlag=True
    try:
        mykey = regctrl.OpenKey(regctrl.HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System")
    except EnvironmentError:
        print "Registry not locked! Pass! "
        lockFlag=False
    if lockFlag==True:
        regctrl.SetValueEx(mykey, 'DisableRegistryTools', regctrl.REG_DWORD, "00000000")
        mykey.close()
    pass

### Start Main Function
def main():
    print " ** Welcome to automatic restore tool Version 1.3b ** "
    desire = raw_input("What address do you desire to restore: ")
    print "Test if registry has been locked.. "
    unlockReg()
    print "Restore Homepage.. "
    restoreReg1(desire)
    restoreReg2()
    print "Done! "
    print "Copyright (C) S Xia 2014"
    raw_input("Press any key to exit.. ")
#######################

### Universal Controller
if __name__ == '__main__':
    main()
########################